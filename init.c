#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "philo.h"

t_philo		*g_first = NULL;
t_philo		*g_last = NULL;

void		init_sticks(t_philo *philo)
{
  if (pthread_mutex_init(&philo->mutex, NULL) != 0)
    {
      perror("mutex_init");
      exit(0);
    }
}

t_philo		*init_list(int i)
{
  t_philo	*philo;
  static char	names[7][17] = {
    {"Platon"}, {"Descartes"}, {"Kant"},
    {"Spinoza"}, {"Montaigne"}, {"Rousseau"},
    {"Aristote"}
  };

  philo = malloc(sizeof(t_philo));
  if (!philo)
    {
      perror("malloc");
      exit (EXIT_FAILURE);
    }
  philo->name = names[i];
  philo->state = REST;
  philo->prev = g_last;
  philo->next = NULL;
  if (g_last)
    g_last->next = philo;
  else
    g_first = philo;
  g_last = philo;
  return (philo);
}

void		init_philo()
{
  t_philo	*philo;
  int		i;

   philo = malloc(sizeof(t_philo));
   if (!philo)
    {
      perror("malloc");
      exit (EXIT_FAILURE);
    }
   i = 0;
   while (i < NB_PHILO)
     {
       philo = init_list(i);
       init_sticks(philo);
       pthread_create(&philo->phil, NULL, thread_1, philo);
       i++;
     }
   philo->next = g_first;
   philo->next->prev = g_last;
}

void		launch()
{
  int		i;
  t_philo	*philo;

  i = 0;
  philo = g_first;
  while (i < NB_PHILO)
    {
      pthread_join(philo->phil, NULL);
      i++;
      philo = philo->next;
    }
}
