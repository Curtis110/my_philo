NAME	= philo

SRC	= main.c \
	init.c

RM	= rm -f

CC	= gcc

OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -Werror

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) -pthread

all: $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
