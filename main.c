#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "philo.h"

int		g_rice = NB_RICE;

void		philo_sleep(t_philo *philo)
{
  if (philo->state == REST)
    return ;
  printf("\033[34;01mLe philosophe %s se repose !\033[00m\n", philo->name);
  philo->state = REST;
  usleep(10000);
}

void		eat(t_philo *philo)
{
  if (g_rice <= 0)
    {
      printf("\033[33;01m%s va acheter du riz !\033[00m\n", philo->name);
      exit (EXIT_FAILURE);
    }
  g_rice--;
  printf("\033[31;01m%s mange ! riz : %d\033[00m\n", philo->name, g_rice);
  philo->state = EAT;
  usleep(10000);
  pthread_mutex_unlock(&philo->mutex);
  pthread_mutex_unlock(&philo->next->mutex);
  philo_sleep(philo);
}

void		think(t_philo *philo, int right)
{
  printf("\033[32;01mLe philosophe %s réflechit !\033[00m\n", philo->name);
  philo->state = THINK;
  usleep(10000);
  if (right == 0)
    pthread_mutex_lock(&philo->mutex);
  else
    pthread_mutex_lock(&philo->next->mutex);
  eat(philo);
}

void		*thread_1(void *arg)
{
  t_philo	*philo;
  int		left_stick;
  int		right_stick;

  philo = (t_philo *)arg;
  while (g_rice > 0)
    {
      left_stick = pthread_mutex_trylock(&philo->mutex);
      right_stick = pthread_mutex_trylock(&philo->next->mutex);
      if ((left_stick == 0 && right_stick == 0) && (philo->state != EAT))
	eat(philo);
      else if ((right_stick) == 0 && (philo->next->state != THINK)
	       && (philo->state != THINK))
	think(philo, right_stick);
      else if ((left_stick) == 0 && (philo->prev->state != THINK)
	       && (philo->state != THINK))
	think(philo, right_stick);
      else
	philo_sleep(philo);
    }
  pthread_exit(NULL);
  return (NULL);
}

int		main(void)
{
  printf("___Les philosophes se réveillent !!___\n");
  sleep(3);
  init_philo();
  launch();
  return (EXIT_SUCCESS);
}
