#ifndef PHILO_H_
# define PHILO_H_

# include <pthread.h>

# define FREE 1
# define TAKEN 0
# define REST 1
# define EAT 2
# define THINK 3
# define NB_RICE 100
# define NB_PHILO 7

typedef struct		s_philo
{
  pthread_t		phil;
  char			*name;
  int			prev_state;
  int			state;
  pthread_mutex_t	mutex;
  struct s_philo	*next;
  struct s_philo	*prev;
}			t_philo;

void init_philo();
t_philo *init_list();
void init_sticks(t_philo *philo);
void *thread_1(void *arg);
void launch();

#endif /* !PHILO_H_ */
